﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EchoServer.Service;

namespace EchoServer.Infrastructure
{
    public class RoomService:IRoomService
    {
        private readonly int _removeDelayTimeMs;
        private readonly Dictionary<string, RoomInfo> _rooms = new Dictionary<string, RoomInfo>();
        private readonly Timer _timer;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="removeDelayTimeMs">Remove unused room delay, in ms</param>
        public RoomService(int removeDelayTimeMs)
        {
            _removeDelayTimeMs = removeDelayTimeMs;
            _timer = new Timer(x => CheckRooms(), null, 0, _removeDelayTimeMs);
        }

        private void CheckRooms()
        {
            lock (_rooms)
            {
                var checkDate = DateTime.Now.AddMilliseconds(-_removeDelayTimeMs);
                var deleteRooms = _rooms.Where(x =>
                {
                    lock (x.Value.Clients)
                    {
                        return x.Value.LastActionDate <= checkDate;
                    }
                }).ToList();

                deleteRooms.ForEach(x=>_rooms.Remove(x.Key));
            }
        }

        public void JoinRoom(string roomName, string clientId)
        {
            RoomInfo roomInfo;
            lock (_rooms)
            {
                if (!_rooms.TryGetValue(roomName, out roomInfo))
                {
                    roomInfo = new RoomInfo();
                    _rooms.Add(roomName, roomInfo);
                }
            }

            lock (roomInfo.Clients)
            {
                roomInfo.Clients.Add(clientId);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="roomName"></param>
        /// <param name="clientId"></param>
        /// <param name="action">Returns ClientId list of the room to the action</param>
        /// <returns></returns>
        public bool RoomDoAction(string roomName, string clientId, Action<IList<string>> action)
        {
            RoomInfo roomInfo;
            lock (_rooms)
            {
                if (!_rooms.TryGetValue(roomName, out roomInfo))
                {
                    return false;
                }
            }

            IList<string> clients;
            lock (roomInfo.Clients)
            {
                if (!roomInfo.Clients.Contains(clientId)) return false;

                clients = roomInfo.Clients.ToList();
                roomInfo.LastActionDate = DateTime.Now;
            }

            //Pass clients copy to avoid editing
            action(clients);
            return true;
        }

        public void LeaveAllRooms(string clientId)
        {
            lock (_rooms)
            {
                foreach (var room in _rooms)
                {
                    lock (room.Value.Clients)
                    {
                        room.Value.Clients.Remove(clientId);
                    }
                }
            }
        }

        private class RoomInfo
        {
            public DateTime LastActionDate { get; set; } = DateTime.Now;
            public HashSet<string> Clients { get; } = new HashSet<string>();
        }
    }
}
