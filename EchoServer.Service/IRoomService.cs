﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EchoServer.Service
{
    public interface IRoomService
    {
        void JoinRoom(string roomName, string clientId);
        void LeaveAllRooms(string clientId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="roomName"></param>
        /// <param name="clientId"></param>
        /// <param name="action">Returns ClientId list of the room to the action</param>
        /// <returns></returns>
        bool RoomDoAction(string roomName, string clientId, Action<IList<string>> action);
    }
}
