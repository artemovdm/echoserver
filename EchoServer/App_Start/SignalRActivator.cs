﻿using System;
using System.Collections.Generic;
using EchoServer;
using EchoServer.App_Start;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Microsoft.Practices.Unity;
using Owin;

namespace EchoServer
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var container = UnityConfig.GetConfiguredContainer();
            // used for SignalR
            GlobalHost.DependencyResolver = new SignalRUnityDependencyResolver(container);

            app.MapSignalR();
        }

        #region SignalRUnityDependencyResolver
        public class SignalRUnityDependencyResolver : DefaultDependencyResolver
        {
            private readonly IUnityContainer _container;

            public SignalRUnityDependencyResolver(IUnityContainer container)
            {
                if (container == null)
                {
                    throw new ArgumentNullException(nameof(container), "Container cannot be null");
                }
                _container = container;
            }

            public override object GetService(Type serviceType)
            {
                return _container.IsRegistered(serviceType) ? _container.Resolve(serviceType) : base.GetService(serviceType);
            }

            public override IEnumerable<object> GetServices(Type serviceType)
            {
                return _container.IsRegistered(serviceType) ? _container.ResolveAll(serviceType) : base.GetServices(serviceType);
            }
        }
        #endregion
    }
}