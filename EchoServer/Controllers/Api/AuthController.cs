﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using EchoServer.Models;
using EchoServer.Service;

namespace EchoServer.Controllers.Api
{
    public class AuthController : ApiController
    {
        const string ONLINE_MESSAGE = "EchoServer api online";

        // GET: api/Auth
        public string Get()
        {
            return ONLINE_MESSAGE;
        }

        [HttpPost]
        public bool Login(AuthParams @params)
        {
            if (string.IsNullOrEmpty(@params.Login)) return false;

            FormsAuthentication.SetAuthCookie(@params.Login.Trim(), true);
            return true;
        }
        [HttpPost]
        public bool Logout()
        {
            FormsAuthentication.SignOut();

            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, string.Empty)
            {
                Expires = DateTime.Now.AddYears(-1)
            };
            HttpContext.Current.Response.Cookies.Set(cookie);

            return true;
        }
        [HttpGet]
        public bool IsAuthenticated()
        {
            return RequestContext.Principal.Identity.IsAuthenticated;
        }
    }
}
