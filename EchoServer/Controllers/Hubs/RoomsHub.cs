﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using EchoServer.Service;
using Microsoft.AspNet.SignalR;

namespace EchoServer.Controllers.Hubs
{
    
    public class RoomsHub : Hub
    {
        private readonly IRoomService _roomService;

        public RoomsHub(IRoomService roomService)
        {
            _roomService = roomService;
        }
        [Authorize]
        public bool JoinRoom(string roomName)
        {
            if (string.IsNullOrWhiteSpace(roomName)) return false;

            _roomService.JoinRoom(roomName, Context.ConnectionId);
            return true;
        }
        [Authorize]
        public bool SendMessage(string roomName, string message)
        {
            if (string.IsNullOrWhiteSpace(roomName)) return false;

            return _roomService.RoomDoAction(roomName, Context.ConnectionId, 
                x => Clients.Clients(x).receiveMessage(roomName, $"{Context.User.Identity.Name}[{DateTime.Now.ToString("T")}]: {message}"));
        }
        
        public override Task OnDisconnected(bool stopCalled)
        {
            return Task.Run(()=>_roomService.LeaveAllRooms(Context.ConnectionId));
        }
    }
}