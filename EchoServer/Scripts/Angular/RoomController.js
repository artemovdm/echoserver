﻿app.controller('RoomController', ['$scope', '$http', '$timeout', '$interval', '$sce', function ($scope, $http, $timeout, $interval, $sce) {
    var _maxRoomMessages = 200;
    var _minRoomMessages = 100;

    $scope.data = {};
    $scope.rooms = [];
    $scope.intervals = [];

    $scope.roomsHub = $.connection.roomsHub;
    

    $scope.roomsHub.client.receiveMessage = function(roomName, message) {
        var room = $.grep($scope.rooms, function(room) {return room.name === roomName})[0];
        if (room) {
            $timeout(function() {
                room.messages.splice(0,0,message);
                if (room.messages.length > _maxRoomMessages) {
                    room.messages.splice(_minRoomMessages, room.messages.length - _minRoomMessages);
                }
            });
        }
    };

    $scope.init = function (isAuthenticated, login) {
        $scope.isAuthenticated = isAuthenticated;
        $scope.login = login;
    }

    $scope.doLogin = function (login) {
        $http.post("/api/auth/login", { login: login }).then(function (res) {
            if (res.data) {
                $scope.login = login;
                $scope.isAuthenticated = true;
            } else {
                alert('Unable to login');
            }
        });
    }
    $scope.doLogout = function () {
        $.connection.hub.stop();
        $scope.rooms = [];
        $scope.intervals.forEach(function(x) {$interval.cancel(x)});
        $scope.intervals = [];

        $http.post("/api/auth/logout").then(function() {
            $scope.login = "";
            $scope.isAuthenticated = false;
        });


    }

    $scope.doJoin = function (joinRoom) {
        var roomExists = $.grep($scope.rooms, function (room) { return room.name === joinRoom }).length;
        if (roomExists) return;

        $scope.roomsHub.server.joinRoom(joinRoom).then(function (res) {
            if (!res) return;

            $scope.rooms.push({ name: joinRoom, messages: [], htmlList: $scope.htmlList });

            var stopTime = $interval(function () {
                $scope.roomsHub.server.sendMessage(joinRoom, "echo");
            }, 100);

            $scope.intervals.push(stopTime);
        });
    }
    $scope.htmlList = function (array) {
        return $sce.trustAsHtml(array.join('<br/>'));
    }
    
    $scope.$watch('isAuthenticated', function(val) {
        if (val) {
            $.connection.hub.start().fail(function() {
                alert("Unable to establish connection to server.");
            });
        } else {
            $.connection.hub.stop();
        }
    })
}]);